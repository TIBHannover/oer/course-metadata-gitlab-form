# Course Metadata GitLab Form

Simple user interface to collect (partial) standard metadata and create a yml file.

* simple HTML/JS file, that form entries into a yml format

[Hier das alte Formular](https://tibhannover.gitlab.io/oer/course-metadata-gitlab-form/metadata-generator.html)

[Hier das neue Formular](https://tibhannover.gitlab.io/oer/course-metadata-gitlab-form/mdg_oer_ntm_de.html)
