var metadataGenerator=(function (){
	var requiredIndicator=0;
	var init=function(){
		requiredIndicator=0;
		metadataGenerator.binePageEvents();
		metadataGenerator.loadLabels();
		metadataGenerator.loadPlaceHolders();
		metadataGenerator.applyLicenceColor();
	};
	var applyLicenceColor=function(){
		var idxLicence=dictionary_metadata.optionValues.licence.indexOf($("select[title='licence']").val());
		var color=dictionary_metadata.optionValues.licenceValues[idxLicence].color;
		for (var i = 0; i < dictionary_metadata.optionValues.licenceValues.length; i++) {
			var c = dictionary_metadata.optionValues.licenceValues[i].color;
			if (c != color) {
				$("select[title='licence']").removeClass( c );
			}
		}
		if (color) {
			$("select[title='licence']").addClass( color );
		}
	};
	var binePageEvents=function(){
		$(".today").datepicker();
		$(".today" ).datepicker( "setDate", new Date() );
		$("select[title='educationalInstitution']").val(dictionary_metadata.defaultValues.educationalInstitution);
		$("select[title='language']").val(dictionary_metadata.defaultValues.language);
		$("select[title='licence']").val(dictionary_metadata.defaultValues.licence);
		$("select[id='licenceSelect'] option").each(function() {
			var idxLicence=dictionary_metadata.optionValues.licence.indexOf($(this).val());
			if (idxLicence >= 0) {
				var color=dictionary_metadata.optionValues.licenceValues[idxLicence].color;
				if (color) {
					$(this).addClass(color);
				}
			}
		});
		$(".help").off().on("click",function(e){
				$.alert({
				    title: dictionary_metadata.labels.helpHeading,
				    content: dictionary_metadata.helpValues[$(this).attr("title")],
				    theme:"Supervan",
				    columnClass: "small"
				});
				var a=$(this).title;
		});
		$("select[title='licence']").val()
		$("#institutionDpd").off().on("change",function(e){
			var idx=dictionary_metadata.optionValues.institution.indexOf($(this).val());
			$("#institutionRor").val(dictionary_metadata.optionValues.institutionRor[idx]);
		});
		$("#licenceSelect").off().on("change",function(e){
			var idxLicence=dictionary_metadata.optionValues.licence.indexOf($(this).val());
			$("#licenceName").val(dictionary_metadata.optionValues.licenceValues[idxLicence].shortName);
			$("#licenceUrl").val(dictionary_metadata.optionValues.licenceValues[idxLicence].url);
			metadataGenerator.applyLicenceColor();
		});
		$("select[title='language'] option").each(function() {
			var idxLanguage=dictionary_metadata.optionValues.language.indexOf($(this).text());
			$(this).val(dictionary_metadata.optionValues.languageCodeIso639_1[idxLanguage]);
		});
	};
	var loadLabels=function(){
		var labelDict=dictionary_metadata.labels;
		for(label in dictionary_metadata.labels){
			$("#"+label).text(dictionary_metadata.labels[label]);
		}
	};
	var loadPlaceHolders=function(){
		return false;
	};
	return{
		init:init,
		binePageEvents:binePageEvents,
		requiredIndicator:requiredIndicator,
		loadLabels:loadLabels,
		loadPlaceHolders:loadPlaceHolders,
		applyLicenceColor:applyLicenceColor
	}
})();

$(document).ready(function(){
	metadataGenerator.init()
	$("input").eq(0).keyup();
});
